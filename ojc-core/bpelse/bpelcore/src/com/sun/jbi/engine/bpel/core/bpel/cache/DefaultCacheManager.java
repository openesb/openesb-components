/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.engine.bpel.core.bpel.cache;

import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Node;

/**
 *
 * @author david
 */
public class DefaultCacheManager implements CacheManager {
 
    private Map<String, Node> cache = new HashMap<String, Node>();

    public String put(String key, Node value) {
        cache.put(key, value);
        return key;
    }

    public Node get(String key) {
        return cache.get(key);
    }

    public Node remove(String key) {
        return cache.remove(key);
    }
}
